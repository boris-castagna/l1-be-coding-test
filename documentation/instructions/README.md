# L1 BE Coding Test

Thanks for going through our interview process and for taking the coding test!

## Problem description

There is an aggregated log file that contains log lines from multiple services. This file is potentially very large. A very small example of this file can be found in the provided `logs.txt`.

We would now like to analyze this file, e.g. count log lines for a specific service.

## Tasks

1. Build a background worker that loads the file to a database. Pick any DB that you are familiar with and decide on a schema. The import should be triggered manually.

2. Build a RESTful service that implements the provided `api.yaml` OpenAPI specs.

The service should query the database and provide a single endpoint `/count` which returns a count of rows that match the filter criteria.

   a) This endpoint accepts a list of filters via GET request and allows zero or more filter parameters. The filters are:
   - serviceNames
   - statusCode
   - startDate
   - endDate
   
   b) Endpoint result:

```
{
    "counter": 1
}
```


## Notes

Please see the attached `api.yaml` (OpenAPI specs) for more information.

For testing purposes take a look at our example log file `logs.txt`. It only contains 20 entries. Your submission will be evaluated with a much larger file.


## Submit your solution

Please create a repository on Github, Gitlab or BitBucket with your solution.

Implement the solution in **PHP 8**, ideally using the **Symfony** framework. **Dockerize the app** and provide a **readme** how to set up and run your application.

Once you are done with the challenge, please send us the link to the repo.

Wishing you the best.


## What we are looking at

1. Correctness
2. SOLID / DRY / KISS
3. Documentation
4. Tests
5. Design patterns
6. Agnostic / Reusable Code
7. Namespaces
8. Packaging using composer
