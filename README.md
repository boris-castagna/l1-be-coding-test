# LegalOne Coding Test

## Author

Boris Castagna - boris.castagna@gmail.com

## Description

This project is the LegalOne BackEnd Coding test.

It uses a custom docker image for PHP & Apache. All the other images are standard (mariadb and adminer).

## Installation

First, install docker-compose according to which Operating system you use by following these instructions :
https://docs.docker.com/compose/install/

Next, you can either build the custom PHP docker image locally and install the app and its dependencies OR you can pull
it already pre-built and packaged from the GitLab project container
registry: https://gitlab.com/boris-castagna/l1-be-coding-test/container_registry

### Build the image locally ...

Execute following commands in order to locally build the custom PHP image :

```
docker build -t registry.gitlab.com/boris-castagna/l1-be-coding-test/php . -f ./infrastructure/docker/images/php/Dockerfile
```

Note: If you encounter weird behavior with the docker build command, add the flag `--no-cache` in order to rebuild all
the image layers entirely.

Ensure the custom PHP image `l1-be-coding-test` is there locally with the tag `latest` with following command:

```
docker images
```

### ... OR Pull the pre-built image from GitLab registry

Generate a GitLab personal token in order to be able to connect to
Gitlab : https://gitlab.com/-/profile/personal_access_tokens
Be sure to check permissions `read_registry` and `write_registry`.

The username is your GitLab profile name.

Example:

```
docker login -u boris-castagna -p YOUR_PERSONAL_TOKEN registry.gitlab.com
```

Once the login is a success, execute the following commands at the root of the project to pull the docker images
locally:

```
docker-compose pull
```

Ensure the pre-built PHP image is there locally with the tag `latest` with following command:

```
docker images
```

### Setup the API

Go to project root folder and launch the containers through docker-compose :

```
docker-compose up -d
```

Connect to the container and install the dependencies :

```
docker exec l1-php composer install
```

Connect to the container and update the database schema :

```
docker exec l1-php php bin/console doctrine:schema:update --force
```

## Usage

Access the API through : http://localhost:30000/api

Access the API Doc through : http://localhost:30000/api/doc

Access the Adminer UI for database: http://localhost:30001/ (use username and password from the docker-compose file)

### Ingest data

In order to ingest data, the logs file must be accessible to the container, so we recommend putting in api/var/ folder
```
docker exec l1-php bin/console legalone:ingest var/example_file.txt -vv
```
Don't forget the verbose option `-vv` as it will allow printing report on the console output.
A good example file is located in `api/tests/Data/logs_with_fails_100k.txt`
```
docker exec l1-php bin/console legalone:ingest tests/Data/logs_with_fails_100k.txt -vv
```

### Query the API

Use the API Doc url or install Postman or even query it directly into browser as it is a GET with security restrictions :
```
http://localhost:30000/api/count
```
Or with parameters (that needs to be URL encoded)
```
http://localhost:30000/api/count?serviceNames%5B%5D=invoice&serviceNames%5B%5D=police&startDate=18%2FAug%2F2019%3A09%3A31%3A56&endDate=25%2FAug%2F2022%3A09%3A31%3A56
```

### Generate test files

In order to test the performance of the process, a test files generating command has been written. Use it like this:
```
docker exec l1-php bin/console legalone:generate-logs-file 100000 -vv
```
It will print out the resulting path file with some uid in the name:
```
File generated : /var/www/html/var/test_logs_52e59b73-a7db-403c-af5d-bf0d33743301.txt
```

## Tests

To run the tests, execute the following command.

```
docker exec l1-php php ./vendor/bin/phpunit
```

2 testing styles :

- Unit Tests (tests/Unit/ folder) : Used to test specific functions. The main tested objects here are the entities and
  the services, as they are the 2 type of objects that holds most of the easily testable logic.

- IntegrationTest (tests/Integration/ folder) : Used to test the whole process and the API controller. A full database
  is booted up and created from scratch before each test on the specific mariadb container. This approach was chosen but
  another one could be: boot the database if not present and clean every table before each test (truncate). This last
  approach can be very cumbersome on a large numbers of tables and foreign keys.

## Notes on Architecture

### System

- The custom PHP Dockerfile is inspired by the PHP Docker image of the Pimcore
  project : https://github.com/pimcore/docker
- Having the control over the PHP Dockerfile is necessary in order to be able to tweak configuration and add any
  additional packages the app would need.
- Symfony version 5.4 was chosen over the very last version (6.X) as it is an LTS version, and it supports PHP 8.

### Code style

- Chosen PHP Code style is set from PSR-12
- PHP8 attributes and syntaxic sugar is used whenever possible

### Main libraries and packages used

- symfony/test-pack : this package is a complete official package for writing unit-tests and
  integrations-tests within symfony project
- mockery/mockery : Mockery is a very easy to use tool to create and provide Mocks in PHP.
- friendsofsymfony/rest-bundle: Probably a little bit overkill for this Coding test, but it works well and does not need a lot 
of configuration to work. ParamFetcher and View Listener are useful.
- nelmio/api-doc-bundle: openAPI doc generator. Can also parse FOSRest annotations, which is convenient.
- doctrine/doctrine-fixtures-bundle: To load the data for integration tests
- Many other smaller libs from symfony's world...

### Failure

The system ingests logs lines. This process is pretty straightforward but in realword conditions, the real added value
is the resilience of the process, i.e its capacity to manage errors without corrupting the data or having to spend time 
in SQL querying or database restoration.

From the instructions given for this project, it was decided that this ingestion process must be able to:

- Handle failed lines without breaking the entire ingestion process,
- Provide adequate reporting (performance and success or failure) to the user,
- Provide an easy way to eventually replay just the failed lines without having to replay the entire file or make
  duplicates into the database,
- Control the rate of failure in real time in order to stop the process and correct the files or the ingestion code if
  necessary (the line regexp for example),

In order to illustrate the error handling, the ingest process validates a line of log against strong constraints. For
example, for a log line to be valid, HTTP Code must have a value between 100 and 599, which is not always the case for a
real world log system (it could be 0 for example as some systems sometimes returns this when the distant server is
unresponsive)

The system as it is designed can handled many failures possibilities and provides a way to recover from these
failures:
 
- Failure per line : writes the lines in error to a specific file, in order to eventually re-ingest them after correction.
- Failure per file : failure rate too high or any other problem, it stops the process and suggests deleting the already added lines.
- Always fixable : as every logline has a report uid (see section below `Report Uid & Foreign Key`), it's easy to delete those lines
and carry on.

### Performance

The system was tested and optimized in order to run with the basic amount of memory that PHP comes configured with (
64Mo).

Performance benchmark is as followed :

| Lines Count | Execution Time | Lines in errors | lines inserted |
|-------------|----------------|-----------------|----------------|
| 10          | less than 75ms | 0               | 10             |
| 100k        | ~16seconds     | ~13k            | ~87k           |
| 2 Million   | ~5 minutes     | ~250k           | 1.75 Million  |

```
root@d89e2b4b38e0:/var/www/html# php bin/console legalone:ingest-logs /var/www/html/var/logs_with_fails_2000k.txt -vv
[info] -- Starting ingest process...
[info] OK file exists
[info] OK file is readable
[info] OK number of lines acceptable (2000001 lines)
[info] Starting ingestion process...
 1000/1000 [============================] 100% 5 mins/5 mins[info] 
[info] -- Finished ingest process. Report 193e4426-659f-4940-8b45-2a952bcbde14 :
[info] -- Total lines: 2000001
[info] -- Success lines: 1750417
[info] -- Failure lines: 249584
[info] -- Wrote file with lines in errors: /var/www/html/var/logs_with_fails_2000k.txt.193e4426-659f-4940-8b45-2a952bcbde14.errors
[info] -- Total time for ingestion prcess: 5 min 5 secs
[info] -- Global Failure rate : 12.48%
```

Given that PHP is probably not the ideal language to solve such a problem, the performance seems acceptable.
A better option would have been to write this program in GO, Python or even RUST.

Optimizing performance includes:

- Reducing SQL Logging capacity
- Controlling and reducing the memory used: writing the lines in errors to a file instead of storing in memory
- Finding a flush() threshold for doctrine's UnitOfWork

A native query could have been written and would probably be the most effective way of insert data into the database but
for the sake of this Coding Test, it was decided to use doctrine and optimize it as much as possible until it's
performance is acceptable.

### Report Uid & Foreign Key

It was chosen to link the logline and report entities without an actual foreign key constraint but with a simple uid.
This way, INSERT performance are not degraded (as foreign keys constraints are checked each time a row is inserted).

### API

API is very simple, one GET endpoint described through OpenAPI Doc. 
It returns JSON only but could return XML or even HTML.

### Security
The instructions for this test did not mention any requirements with regard to security, but it's worth noticing 
that he API, as it is, (endpoint /api/count) is NOT secured in a sense that every request will be authorized.
See the `Security` section in the `Possible Improvements` about this. 

## Possible improvements

### Code

- Validation of dates (in LogLine entity and in Filters DTO) could have been done in a custom validator to avoid 
duplicating the logic in both objects.
- Exceptions, especially in the API controller, could be handled in a Listener to return proper API JSON Response each time 
any exception is thrown. It would avoid duplicating the `try-catch` statement in each controller action.
- Integration tests of the API would need to have another version of the app in TEST env to be able to have true
API Integration tests. As of today, it uses the DEV env, which is not acceptable for a real world solution.

### Architecture

- It is possible to have more control over PHP config (memory allocated for example) by
sharing with the docker-compose config the php.ini file and use it as a volume:
```
./docker/conf/php/php.ini:/usr/local/etc/php/php.ini
```

- In termes of security, a docker-compose commited to the repository should never hold the passwords and definitive values.
It is a good practice to have them stored in the server's ENV VARS and then placeholded into the docker-compose file for each given env.
This way, the source repo have never any password or sensitive data.

- We could have added other improvements to this project:
  - cronjobs, that would scan every minute a folder, in the same way Java Application Servers usually
  deploy WARs and JARs, by hot deploying them.
  - Use RabbitMQ to optimize (see below)
  - Print a report to a file and send it by email
  - autobackup the database before each ingestion process
  - ...

### Performance

To improve the performance of the ingestion process, it would have been possible to parallelize the ingestion process
through RabbitMQ server or any other asynchronous and background mechanism. This could be implemented in the following way:
- Install and configure RabbitMQ server through Docker
- Install and configure Symfony's messenger
- Create a service called for example `DispatchIngestTasksService`. This service would have the responsibility to
split in several other temp files the massive given file and to launch `IngestTasks` for each split file.
- Create an async message called for example `IngestTaskService` that would just launch the existing 
`IngestLogsService` with the given attributed temp file.

With such design, it is essential to think about locks and other database asynchronous usage issues 
that could arise. But the performance would be way better, for example, ingesting 10 millions lines with 100 `IngestTasks` 
running at the same time would be way quicker than ingesting 10 millions lines at once.

### Security

As mentioned earlier in this documentation, no security was required for this exercice for the /count 
endpoint, but it could have been possible to implement one based on JWT, for example, Lexiik/JwtBundle.


