<?php

declare(strict_types=1);

namespace App\Tests\Data\Fixtures;

use App\Entity\LogLine;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TestGetLogLinesByServiceFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $logLine = new LogLine(
            'user',
            '17/Aug/2021:09:21:53',
            '+0000',
            'GET /users HTTP/1.1',
            '404',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);

        $logLine = new LogLine(
            'invoice',
            '17/Aug/2021:09:21:53',
            '+0000',
            'POST /users HTTP/1.1',
            '200',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);

        $logLine = new LogLine(
            'police',
            '17/Aug/2021:09:21:53',
            '+0000',
            'GET /users HTTP/1.1',
            '209',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);
        $logLine = new LogLine(
            'user',
            '17/Aug/2021:09:21:53',
            '+0000',
            'GET /invoices HTTP/1.1',
            '302',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);
        $manager->flush();
    }
}