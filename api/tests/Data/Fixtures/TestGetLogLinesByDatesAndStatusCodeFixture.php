<?php

namespace App\Tests\Data\Fixtures;

use App\Entity\LogLine;
use Doctrine\Persistence\ObjectManager;

class TestGetLogLinesByDatesAndStatusCodeFixture
{
    public function load(ObjectManager $manager)
    {
        // OK
        $logLine = new LogLine(
            'user',
            '17/Aug/2021:09:21:53',
            '+0000',
            'GET /users HTTP/1.1',
            '200',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);

        // KO : service is not user
        $logLine = new LogLine(
            'invoice',
            '18/Aug/2021:09:21:53',
            '+0000',
            'POST /users HTTP/1.1',
            '200',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);

        // KO : httpCode
        $logLine = new LogLine(
            'police',
            '19/Aug/2021:09:21:53',
            '+0000',
            'GET /users HTTP/1.1',
            '404',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);

        // KO : date
        $logLine = new LogLine(
            'user',
            '05/Aug/2021:09:21:53',
            '+0000',
            'GET /invoices HTTP/1.1',
            '200',
            'a1b2c3d4e'
        );
        $manager->persist($logLine);

        $manager->flush();
    }
}