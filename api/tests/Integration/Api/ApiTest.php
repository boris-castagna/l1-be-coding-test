<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use GuzzleHttp\Exception\GuzzleException;

class ApiTest extends LegalOneIntegrationTestCase
{
    /**
     * @throws GuzzleException
     */
    public function testGetLogLinesCount(): void
    {
        // Arrange

        // Act
        // todo this would need more configuration in order to use public/index_test.php and make sure the env is test
        $response = $this->realGuzzleClient->get('http://localhost/api/count');

        // Assert
        $json = json_decode((string)$response->getBody(), true);
        $this->assertArrayHasKey('count', $json);
    }
}