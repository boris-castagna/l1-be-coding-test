<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use App\Entity\IngestReport;
use App\Entity\LogLine;
use App\Model\Enums\IngestProcessStatusEnum;
use App\Service\IngestLogsService;
use Doctrine\ORM\Tools\ToolsException;
use Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class IngestLogsServiceTest extends LegalOneIntegrationTestCase
{
    private IngestLogsService $target;
    private string $testFilePath;

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->target = self::getContainer()->get(IngestLogsService::class);
        $params = self::getContainer()->get(ParameterBagInterface::class);
        $this->testFilePath = $params->get('kernel.project_dir') . '/tests/Data/';
    }

    /**
     * Happy Path test case
     *
     * @throws Exception
     */
    public function testIngestLogsLineWithProvidedFile(): void
    {
        // Arrange
        $file = $this->testFilePath . 'logs.txt';

        // Act
        $report = $this->target->ingestLogs($file);

        // Assert
        $countLines = $this->entityManager->getRepository(LogLine::class)->count([]);
        $this->assertEquals(20, $countLines);

        $this->assertEquals(IngestProcessStatusEnum::SUCCESS, $report->getStatus());
        $this->assertEquals(20, $report->getTotalLinesCount());
        $this->assertEquals(20, $report->getSuccessLinesCount());
        $this->assertEquals(0, $report->getFailureLinesCount());
    }

    /**
     * As numbers here are pre-computed and verified manually this test will be useful as a verification
     * in the CI for example in case the ingestion process evolves and breaks these numbers
     *
     * @throws Exception
     */
    public function testIngestLogsLineWithFailsAnd100kLines()
    {
        // Arrange
        $file = $this->testFilePath . 'logs_with_fails_100k.txt';
        $successLinesCount = 87439; // pre-computed and verified manually
        $failuresLinesCount = 12561; // pre-computed and verified manually

        // Act
        $report = $this->target->ingestLogs($file);

        // Assert
        $countLines = $this->entityManager->getRepository(LogLine::class)->count([]);
        $this->assertEquals($successLinesCount, $countLines);

        $this->assertEquals(IngestProcessStatusEnum::SUCCESS, $report->getStatus());
        $this->assertEquals(100000, $report->getTotalLinesCount());
        $this->assertEquals($successLinesCount, $report->getSuccessLinesCount());
        $this->assertEquals($failuresLinesCount, $report->getFailureLinesCount());
        $this->assertLessThan(IngestReport::ACCEPTABLE_FAILURE_RATE, $report->computeFailureRate());

        // Check that error file is written (and then delete it)
        // TODO improve this and specify temp path for test env instead of writing into data of test folder directly
        $errorsFilePath = $file . '.' . $report->getUid() . '.errors';
        if (!file_exists($errorsFilePath)) {
            $this->fail("Could not find errors file $errorsFilePath");
        }

        // Count the number of lines written in errors file
        $count = 0;
        $handle = fopen($errorsFilePath, "r");
        while (!feof($handle)) {
            fgets($handle);
            $count++;
        }
        fclose($handle);
        unlink($errorsFilePath);
        $this->assertEquals($failuresLinesCount, $count - 1); // -1 here as file has empty line at the end
    }
}