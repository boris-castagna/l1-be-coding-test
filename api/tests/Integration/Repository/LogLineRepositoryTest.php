<?php

declare(strict_types=1);

namespace App\Tests\Unit\Repository;

use App\Model\Dto\ApiFiltersDto;
use App\Repository\LogLineRepository;
use App\Tests\Data\Fixtures\TestGetLogLinesByDatesAndStatusCodeFixture;
use App\Tests\Data\Fixtures\TestGetLogLinesByServiceFixture;
use App\Tests\Integration\LegalOneIntegrationTestCase;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class LogLineRepositoryTest extends LegalOneIntegrationTestCase
{
    private LogLineRepository $target;

    public function setUp(): void
    {
        parent::setUp();
        $this->target = self::getContainer()->get(LogLineRepository::class);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function testGetLogLinesByService(): void
    {
        // Arrange
        $fixture = new TestGetLogLinesByServiceFixture();
        $fixture->load($this->entityManager);

        $dtoFilters = new ApiFiltersDto(
            ['user']
        );

        // Act
        $result = $this->target->countLinesWithFilters($dtoFilters);

        // Assert
        $this->assertEquals(2, $result);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function testGetLogLinesByDatesAndStatusCodeFixture(): void
    {
        // Arrange
        $fixture = new TestGetLogLinesByDatesAndStatusCodeFixture();
        $fixture->load($this->entityManager);

        $dtoFilters = new ApiFiltersDto(
            ['user'],
            '200',
            '15/Aug/2021:00:00:00',
            '20/Aug/2021:00:00:00'
        );

        // Act
        $result = $this->target->countLinesWithFilters($dtoFilters);

        // Assert
        $this->assertEquals(1, $result);
    }
}