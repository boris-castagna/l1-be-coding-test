<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use GuzzleHttp\Client;
use LogicException;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class LegalOneIntegrationTestCase
 * @package Tests
 */
class LegalOneIntegrationTestCase extends WebTestCase
{
    use MockeryPHPUnitIntegration;

    protected EntityManagerInterface $entityManager;
    protected array $fixtures = [];

    protected ?Client $realGuzzleClient;
    protected ?KernelBrowser $webTestClient;

    /**
     * @throws ToolsException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->webTestClient = self::createClient(); // boots kernel

        if ('test' !== self::$kernel->getEnvironment()) {
            throw new LogicException('Execution only in Test environment possible!');
        }

        $em = self::$kernel->getContainer()->get('doctrine')->getManager();
        $schemaTool = new SchemaTool($em);
        $metadata = $em->getMetadataFactory()->getAllMetadata();

        // Drop and recreate tables for all entities
        $schemaTool->dropSchema($metadata);
        $schemaTool->createSchema($metadata);

        $this->realGuzzleClient = new Client();
        $this->entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        static::getContainer()->get(LoggerInterface::class)->info('-- ');
        static::getContainer()->get(LoggerInterface::class)->info('-- Running test : ' . $this->getName());
        static::getContainer()->get(LoggerInterface::class)->info('-- ');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        Mockery::close();
    }

    /**
     * Replaces the services in the TestContainer.
     * The replaced service must be declared public in services_test.yml
     *
     * @param array $services
     */
    protected function loadServicesReplacements(array $services): void
    {
        self::bootKernel();
        foreach ($services as $alias => $replacement) {
            static::getContainer()->set($alias, $replacement);
        }
    }
}
