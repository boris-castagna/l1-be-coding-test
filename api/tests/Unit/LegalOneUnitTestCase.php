<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\MockInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class LegalOneUnitTestCase
 * @package Tests
 */
class LegalOneUnitTestCase extends KernelTestCase
{
    use MockeryPHPUnitIntegration;

    protected MockInterface $registry;
    protected MockInterface $manager;

    public function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    protected static function createKernel(array $options = []) : KernelInterface
    {
        $class = self::getKernelClass();
        return new $class('test', true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        \Mockery::close();
    }

    protected function mockEntityManager(): void
    {
        $this->registry = \Mockery::mock(ManagerRegistry::class);
        $this->manager = \Mockery::mock(EntityManagerInterface::class);

        $this->manager->shouldReceive('persist')
            ->andReturnUsing(function ($arg) {
                return $arg;
            });
        $this->manager->shouldReceive('flush')->andReturnNull();

        $this->registry->shouldReceive('getManager')
            ->andReturn($this->manager);
    }
}
