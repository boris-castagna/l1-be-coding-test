<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model;

use App\Model\Dto\ApiFiltersDto;
use App\Tests\Unit\LegalOneUnitTestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiFiltersDtoTest extends LegalOneUnitTestCase
{
    private ValidatorInterface $validator;

    public function setUp(): void
    {
        parent::setUp();
        // For once, we want the REAL validator service and not a mock.
        // This service is outside our code so it's ok not to mock it and it's the very purpose of this test
        // to live test the validation, otherwise if validation was mocked, this test would be pretty useless.
        $this->validator = static::getContainer()->get(ValidatorInterface::class);
    }

    public function testCanCreate(): void
    {
        // Act
        $target = new ApiFiltersDto();

        // Assert
        $this->assertInstanceOf(ApiFiltersDto::class, $target);
    }

    public function testInvalidDataHttpCode(): void
    {
        // Arrange
        $target = new ApiFiltersDto(['user'], '999');

        // Act
        $violations = $this->validator->validate($target);

        // Assert
        $this->assertInstanceOf(ApiFiltersDto::class, $target);
        $this->assertCount(1, $violations);
        /** @var ConstraintViolation $violation */
        $violation = $violations[0];
        $this->assertEquals('HTTP Code must be between 100 and 599', $violation->getMessage());
    }

    public function testCanCreateWithNotLogicDates(): void
    {
        // Arrange
        $target = new ApiFiltersDto(
            ['user', 'police'],
            '204',
            '25/Aug/2021:09:29:11',
            '17/Aug/2021:09:29:11'
        );

        // Act
        $violations = $this->validator->validate($target);

        // Assert
        $this->assertInstanceOf(ApiFiltersDto::class, $target);
        $this->assertCount(1, $violations);
        /** @var ConstraintViolation $violation */
        $violation = $violations[0];
        $this->assertEquals('End Date must be greater than Start Date', $violation->getMessage());
    }

    public function testCanCreateWithInvalidDates(): void
    {
        // Expect
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(
            'Error during start date creation with format ' . ApiFiltersDto::DATE_TIME_FORMAT .
            'and value: INVALIDDATE:09:29:11'
        );

        // Act
        $target = new ApiFiltersDto(
            ['user', 'police'],
            '204',
            'INVALIDDATE:09:29:11'
        );
    }

    public function testCanCreateWithValidData(): void
    {
        // Arrange
        $target = new ApiFiltersDto(
            ['UsEr', 'PoLiCe'],
            '204',
            '17/Aug/2021:09:29:11',
            '25/Aug/2021:09:29:11'
        );

        // Act
        $violations = $this->validator->validate($target);

        // Assert
        $this->assertInstanceOf(ApiFiltersDto::class, $target);
        $this->assertCount(0, $violations);
        $this->assertContains('user', $target->getServiceNames());
        $this->assertNotContains('UsEr', $target->getServiceNames());
        $this->assertContains('police', $target->getServiceNames());
        $this->assertNotContains('PoLiCe', $target->getServiceNames());
        $this->assertInstanceOf(\DateTime::class, $target->getStartDate());
        $this->assertInstanceOf(\DateTime::class, $target->getEndDate());
        $this->assertEquals(204, $target->getStatusCode());
    }
}