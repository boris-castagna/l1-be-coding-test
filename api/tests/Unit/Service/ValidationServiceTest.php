<?php

namespace App\Tests\Unit\Service;

use App\Entity\LogLine;
use App\Service\ValidationService;
use App\Tests\Unit\LegalOneUnitTestCase;

class ValidationServiceTest extends LegalOneUnitTestCase
{
    private ValidationService $target;

    public function setUp(): void
    {
        parent::setUp();
        $this->mockEntityManager();
        $this->target = self::getContainer()->get(ValidationService::class);
    }

    /**
     * @throws \Exception
     */
    public function testValidate()
    {
        // Arrange
        $line = new LogLine(
            'user-test',
            '17/Aug/2021:11:38:51',
            '+0200',
            'POST /users HTTP/1.1',
            '999',
            '1a2b3c4d5e'
        );

        // Expect
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('httpStatusCode: HTTP Code must be lesser than 599');

        // Act
        $this->target->validateEntity($line);
    }
}