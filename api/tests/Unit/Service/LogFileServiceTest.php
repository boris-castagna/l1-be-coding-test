<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Service\LogFileService;
use App\Tests\Unit\LegalOneUnitTestCase;
use Mockery\MockInterface;
use Psr\Log\LoggerInterface;

class LogFileServiceTest extends LegalOneUnitTestCase
{
    private LogFileService $target;
    private MockInterface $loggerMock;

    public function setUp(): void
    {
        parent::setUp();
        $this->mockEntityManager();
        $this->loggerMock = \Mockery::mock(LoggerInterface::class);
        $this->target = new LogFileService(
            $this->loggerMock
        );
    }

    public function testCanCreateService(): void
    {
        // Assert
        $this->assertNotNull($this->target);
    }

    public function testFileDoesNotExist(): void
    {
        // Arrange
        $filePath = '/some/file/that/does/not/exist';

        // Expect
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Error: file does not exist : $filePath");

        // Act
        $this->target->performChecksOnFile($filePath);
    }

    /**
     * Note: This test is pretty useless as the container runs as root, who can do anything on a filesystem.
     * If the user that runs the container changes in the future, it would be useful to keep
     * is_readable() check and test it properly.
     */
    public function testFileNotReadableAsRoot(): void
    {
        // Arrange
        $filePath = '/tmp/testfile.txt';
        $file = fopen($filePath, "w");
        if (!$file) {
            $this->fail("Could not create test fail in $filePath");
        }
        chmod($filePath, 0333); // = -wx-wx-wx

        $this->loggerMock->shouldReceive('info');

        // Expect
//        $this->expectException(\RuntimeException::class);
//        $this->expectExceptionMessage("Error: file is not readable : $filePath");

        // Act
        $this->target->performChecksOnFile($filePath);

        // Clean
        unlink($filePath);
    }

    public function testCheckAndReturnLinesCount(): void
    {
        // Arrange
        $filePath = '/tmp/testfile.txt';
        $linesCount = LogFileService::MAX_LINES_THRESHOLD - 1000;
        $this->writeLinesToTestFile($filePath, $linesCount);
        $this->loggerMock->shouldReceive('info');

        // Act
        $result = $this->target->checkAndReturnLinesCount($filePath);

        // Assert
        $this->assertEquals($linesCount, $result);

        // Clean
        unlink($filePath);
    }

    public function testCheckAndReturnLinesCountTooManyLines(): void
    {
        // Arrange
        $filePath = '/tmp/testfile.txt';
        $linesCount = LogFileService::MAX_LINES_THRESHOLD + 1000;
        $this->writeLinesToTestFile($filePath, $linesCount);
        $this->loggerMock->shouldReceive('info');

        // Expect
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Error: file is too large, use the mass ingest method instead ($linesCount lines)");

        // Act
        $this->target->checkAndReturnLinesCount($filePath);

        // Clean
        unlink($filePath);
    }

    private function writeLinesToTestFile(string $filePath, int $linesCount): void
    {
        $file = fopen($filePath, "w");
        if (!$file) {
            $this->fail("Could not create test fail in $filePath");
        }
        for ($i = 1; $i < $linesCount; $i++) {
            fwrite($file, "Test Line \n");
        }
        fclose($file);
    }
}