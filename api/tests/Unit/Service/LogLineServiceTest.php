<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Entity\LogLine;
use App\Service\LogLineBuilder;
use App\Tests\Unit\LegalOneUnitTestCase;

class LogLineServiceTest extends LegalOneUnitTestCase
{
    private LogLineBuilder $target;

    public function setUp(): void
    {
        parent::setUp();
        $this->mockEntityManager();
        $this->target = new LogLineBuilder();
    }

    public function testCanCreateService()
    {
        $this->assertNotNull($this->target);
    }

    /**
     * @throws \Exception
     */
    public function testBuildLogLineMalformation()
    {
        // Arrange
        $line = 'USER-SERVICE - - [BAD LOG LINE sers HTTP/1.1" 204';

        // Expect
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Line log did not pass regexp : needed 6 matching groups found 0');

        // Act
        $this->target->buildFromRawLine($line, 'a1b2c3d4e');
    }

    /**
     * @throws \Exception
     */
    public function testBuildLogLine()
    {
        // Arrange
        $line = 'USER-SERVICE - - [17/Aug/2021:09:26:51 +0700] "POST /users HTTP/1.1" 204';

        // Act
        $result = $this->target->buildFromRawLine($line, 'a1b2c3d4e');

        // Assert
        $this->assertInstanceOf(LogLine::class, $result);
        $this->assertEquals('user', $result->getService());
        $this->assertInstanceOf(\DateTime::class, $result->getDate());
        $this->assertEquals('17/08/2021', $result->getDate()->format('d/m/Y'));
        $this->assertEquals('09:26:51', $result->getDate()->format('H:i:s'));
        $this->assertEquals('+07:00', $result->getDate()->getTimezone()->getName());
        $this->assertEquals('a1b2c3d4e', $result->getReportUid());
        $this->assertEquals('POST /users HTTP/1.1', $result->getContent());
        $this->assertEquals(204, $result->getHttpStatusCode());
    }
}