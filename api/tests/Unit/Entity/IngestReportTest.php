<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Entity\IngestReport;
use App\Tests\Unit\LegalOneUnitTestCase;

class IngestReportTest extends LegalOneUnitTestCase
{

    public function testCanCreateIngestReport(): void
    {
        // Act
        $target = new IngestReport(1000);

        // Assert
        $this->assertNotNull($target);
    }

    public function testComputeFailureRateWithZeroLines(): void
    {
        // Arrange
        $target = new IngestReport(0);

        // Act & assert
        $this->assertEquals(0, $target->computeFailureRate());
    }

    public function testFailureRateAcceptable(): void
    {
        // Arrange
        $target = new IngestReport(100);
        for ($i = 1; $i < IngestReport::ACCEPTABLE_FAILURE_RATE; $i++) {
            $target->increaseFailedLines();
        }

        for ($i = 1; $i < $target->getTotalLinesCount() - $target->getFailureLinesCount(); $i++) {
            $target->increaseSuccessLines();
        }

        // Act & assert
        $this->assertTrue($target->isFailureRateAcceptable());
    }

    public function testFailureRateNotAcceptable(): void
    {
        // Arrange
        $target = new IngestReport(100);
        for ($i = 1; $i < IngestReport::ACCEPTABLE_FAILURE_RATE + 2; $i++) {
            $target->increaseFailedLines();
        }

        for ($i = 1; $i < $target->getTotalLinesCount() - $target->getFailureLinesCount(); $i++) {
            $target->increaseSuccessLines();
        }

        // Act & assert
        $this->assertFalse($target->isFailureRateAcceptable());
    }

    public function testTotalExecutionTimeToDisplayMilliseconds(): void
    {
        // Arrange
        $target = new IngestReport(100);
        $target->setStartDate(new \DateTime('27-03-2022 05:56:22.282777'));
        $target->setEndDate(new \DateTime('27-03-2022 05:56:22.289222'));

        // Act & Assert
        $this->assertEquals('6.445 milliseconds', $target->getTotalExecutionTimeToDisplay());
    }

    public function testTotalExecutionTimeToDisplaySeconds(): void
    {
        // Arrange
        $target = new IngestReport(100);
        $target->setStartDate(new \DateTime('27-03-2022 05:56:22.362777'));
        $target->setEndDate(new \DateTime('27-03-2022 05:56:25.289222'));

        // Act & Assert
        $this->assertEquals('3 seconds', $target->getTotalExecutionTimeToDisplay());
    }

    public function testTotalExecutionTimeToDisplayMinutesAndSecs(): void
    {
        // Arrange
        $target = new IngestReport(100);
        $target->setStartDate(new \DateTime('27-03-2022 05:56:22.962777'));
        $target->setEndDate(new \DateTime('27-03-2022 05:59:25.289222'));

        // Act & Assert
        $this->assertEquals('3 min 3 secs', $target->getTotalExecutionTimeToDisplay());
    }

    public function testTotalExecutionTimeToDisplayHoursMinutesAndSecs(): void
    {
        // Arrange
        $target = new IngestReport(100);
        $target->setStartDate(new \DateTime('27-03-2022 05:56:22.962777'));
        $target->setEndDate(new \DateTime('27-03-2022 07:09:01.289222'));

        // Act & Assert
        $this->assertEquals('1 h 12 min 39 secs', $target->getTotalExecutionTimeToDisplay());
    }
}