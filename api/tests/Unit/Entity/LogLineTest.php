<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Entity\LogLine;
use App\Tests\Unit\LegalOneUnitTestCase;

class LogLineTest extends LegalOneUnitTestCase
{
    public function testCanCreateLogLine(): void
    {
        // Act
        $target = new LogLine(
            'user-test',
            '18/Aug/2021:10:26:53',
            '+0200',
            'POST /users HTTP/1.1',
            '204',
            '1a2b3c4d5e'
        );

        // Assert
        $this->assertNotNull($target);
    }

    public function testCreateWithDateError(): void
    {
        $this->expectException(\RuntimeException::class);

        // Act
        $target = new LogLine(
            'user-test',
            'WRONGDATE:10:26:53',
            '+0200',
            'POST /users HTTP/1.1',
            '204',
            '1a2b3c4d5e'
        );
    }
}