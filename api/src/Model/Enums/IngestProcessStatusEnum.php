<?php

declare(strict_types=1);

namespace App\Model\Enums;

enum IngestProcessStatusEnum: string
{
    case IN_PROGRESS = 'in_progress';
    case SUCCESS = 'success';
    case FAILED = 'failed';
}