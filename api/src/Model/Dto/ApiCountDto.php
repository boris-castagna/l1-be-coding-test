<?php

declare(strict_types=1);

namespace App\Model\Dto;

/**
 * Very small DTO but better for maintenance
 */
class ApiCountDto
{
    private int $count;

    /**
     * @param int $count
     */
    public function __construct(int $count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return ApiCountDto
     */
    public function setCount(int $count): ApiCountDto
    {
        $this->count = $count;
        return $this;
    }
}