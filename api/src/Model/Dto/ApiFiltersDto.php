<?php

declare(strict_types=1);

namespace App\Model\Dto;

use App\Entity\LogLine;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[Assert\Callback(callback: 'validateFilters')]
class ApiFiltersDto
{
    /**
     * Could be centralized with LogLine format but not required or obvious
     * @see LogLine
     */
    public const DATE_TIME_FORMAT = 'd/M/Y:H:i:s';

    private ?array $serviceNames;
    private ?int $statusCode = null;
    private ?\DateTime $startDate = null;
    private ?\DateTime $endDate = null;

    /**
     * Implement Factory or builder pattern if this gets more complicated over time
     *
     * @param array|null $serviceNames
     * @param string|null $statusCode
     * @param string|null $startDate
     * @param string|null $endDate
     */
    public function __construct(
        ?array $serviceNames = null,
        ?string $statusCode = null,
        ?string $startDate = null,
        ?string $endDate = null
    ) {
        if (null !== $serviceNames) {
            $serviceNames = array_map(function ($item) {
                return mb_strtolower($item);
            }, $serviceNames);
        }
        $this->serviceNames = $serviceNames;
        if (null !== $statusCode) {
            $this->statusCode = (int)$statusCode;
        }
        if (null !== $startDate) {
            $this->setStartDateFromString($startDate);
        }
        if (null !== $endDate) {
            $this->setEndDateFromString($endDate);
        }
    }

    /**
     * @return array|null
     */
    public function getServiceNames(): ?array
    {
        return $this->serviceNames;
    }

    /**
     * @param array|null $serviceNames
     * @return ApiFiltersDto
     */
    public function setServiceNames(?array $serviceNames): ApiFiltersDto
    {
        $this->serviceNames = $serviceNames;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    /**
     * @param int|null $statusCode
     * @return ApiFiltersDto
     */
    public function setStatusCode(?int $statusCode): ApiFiltersDto
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param string $date
     * @param bool $throwOnError
     * @return ApiFiltersDto
     */
    public function setStartDateFromString(string $date, bool $throwOnError = true): ApiFiltersDto
    {
        $this->startDate = $this->parseDate($date, $throwOnError);
        return $this;
    }

    /**
     * @param string $date
     * @param bool $throwOnError
     * @return ApiFiltersDto
     */
    public function setEndDateFromString(string $date, bool $throwOnError = true): ApiFiltersDto
    {
        $this->endDate = $this->parseDate($date, $throwOnError);
        return $this;
    }

    /**
     * @param string $date
     * @param bool $throwOnError
     * @return \DateTime
     */
    private function parseDate(string $date, bool $throwOnError = true): \DateTime
    {
        $dateValue = \DateTime::createFromFormat(self::DATE_TIME_FORMAT, $date);
        if (!$dateValue && $throwOnError) {
            throw new \RuntimeException(
                'Error during start date creation with format ' . self::DATE_TIME_FORMAT .
                'and value: ' . $date
            );
        }
        return $dateValue;
    }

    /**
     * As there is some logic between the filters, it is easier to build a custom validation callback
     *
     * @param ExecutionContextInterface $context
     */
    public function validateFilters(ExecutionContextInterface $context): void
    {
        // State date and end date logic
        if (null !== $this->getStartDate() && null !== $this->getEndDate() &&
            $this->getEndDate() < $this->getStartDate()
        ) {
            $context->buildViolation('End Date must be greater than Start Date')
                ->atPath('endDate')
                ->addViolation();
        }

        // HTTP valid code
        if (null !== $this->statusCode && ($this->statusCode < 100 || $this->statusCode > 599)) {
            $context->buildViolation('HTTP Code must be between 100 and 599')
                ->atPath('statusCode')
                ->addViolation();
        }
    }
}