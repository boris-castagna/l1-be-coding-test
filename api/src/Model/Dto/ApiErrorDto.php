<?php

declare(strict_types=1);

namespace App\Model\Dto;

class ApiErrorDto
{
    private string $error;
    private string $details;

    /**
     * @param string $error
     * @param string $details
     */
    public function __construct(string $error, string $details)
    {
        $this->error = $error;
        $this->details = $details;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @param string $error
     * @return ApiErrorDto
     */
    public function setError(string $error): ApiErrorDto
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetails(): string
    {
        return $this->details;
    }

    /**
     * @param string $details
     * @return ApiErrorDto
     */
    public function setDetails(string $details): ApiErrorDto
    {
        $this->details = $details;
        return $this;
    }
}