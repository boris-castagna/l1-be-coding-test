<?php

declare(strict_types=1);

namespace App\Model\Dto;

/**
 * One very small DTO is always better than a plain array
 */
class FailedLogLineDto
{
    private string|bool|null $rawLine;
    private ?string $errors;

    /**
     * @param string|null $errors
     * @param string|bool|null $rawLine
     */
    public function __construct(?string $errors, string|bool|null $rawLine = null)
    {
        $this->rawLine = $rawLine;
        $this->errors = $errors;
    }

    /**
     * @return string|null
     */
    public function getRawLine(): ?string
    {
        return $this->rawLine;
    }

    /**
     * @param string|null $rawLine
     * @return FailedLogLineDto
     */
    public function setRawLine(?string $rawLine): FailedLogLineDto
    {
        $this->rawLine = $rawLine;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getErrors(): ?string
    {
        return $this->errors;
    }

    /**
     * @param string|null $errors
     * @return FailedLogLineDto
     */
    public function setErrors(?string $errors): FailedLogLineDto
    {
        $this->errors = $errors;
        return $this;
    }
}