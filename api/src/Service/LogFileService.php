<?php

declare(strict_types=1);

namespace App\Service;

use Psr\Log\LoggerInterface;

class LogFileService
{
    // Threshold of number of line where the ingest method should be implemented as parallelized through rabbitmq
    public const MAX_LINES_THRESHOLD = 2000000;

    public function __construct(
        private LoggerInterface $logger,
    ) {
    }

    /**
     * @param string $filePath
     */
    public
    function performChecksOnFile(
        string $filePath
    ): void {
        if (!file_exists($filePath)) {
            throw new \RuntimeException("Error: file does not exist : $filePath");
        }
        $this->logger->info('OK file exists');

        if (!is_readable($filePath)) {
            throw new \RuntimeException("Error: file is not readable : $filePath");
        }
        $this->logger->info('OK file is readable');
    }

    /**
     * @param string $filePath
     * @return int
     */
    public function checkAndReturnLinesCount(
        string $filePath
    ): int {
        // Check file size and will error if it is too big
        $count = 0;
        $handle = fopen($filePath, "r");
        while (!feof($handle)) {
            fgets($handle); // fgets() uses a pointer so performance should stay acceptable
            $count++;
        }
        fclose($handle);

        if ($count > self::MAX_LINES_THRESHOLD) {
            throw new \RuntimeException(
                "Error: file is too large, use the mass ingest method instead ($count lines)"
            );
        }
        $this->logger->info("OK number of lines acceptable ($count lines)");

        return $count;
    }
}