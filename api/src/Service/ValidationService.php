<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Wraps the validation service of symfony to be able to throw an exception
 * with all the messages errors at the same time.
 */
class ValidationService
{
    public function __construct(
        private ValidatorInterface $validator,
    ) {
    }

    /**
     * @param mixed $entity
     * @param bool $throwErrors
     * @return ConstraintViolationListInterface
     */
    public
    function validateEntity(
        mixed $entity,
        bool $throwErrors = true
    ): ConstraintViolationListInterface {
        $violations = $this->validator->validate($entity);
        if (count($violations) > 0 && $throwErrors) {
            // Builds the list of errors
            $violationsMessages = null;
            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                empty($violationsMessages) ?: $violationsMessages .= ' | ';
                $violationsMessages .= $violation->getPropertyPath() . ': ' . $violation->getMessage();
            }
            throw new \RuntimeException($violationsMessages);
        }
        return $violations;
    }
}