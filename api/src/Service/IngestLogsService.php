<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\IngestReport;
use App\Model\Enums\IngestProcessStatusEnum;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class IngestLogsService
{
    // Number of lines to be persisted before actually flushing & clearing the unitofwork of loglines
    public const FLUSH_THRESHOLD = 2000;

    private IngestReport $report;
    private ?ProgressBar $progressBar = null;

    /** @var resource|bool $errorsFile */
    private $errorsFile = false; // resource typing is weird...
    private bool $printDebug = false;

    public function __construct(
        private LoggerInterface $logger,
        private ManagerRegistry $registry,
        private LogFileService $logFileService,
        private LogLineBuilder $logLineBuilder,
        private ValidationService $validationService
    ) {
    }

    /**
     * @param string $filePath
     * @param OutputInterface|null $output
     * @return IngestReport
     * @throws \Exception
     */
    public function ingestLogs(string $filePath, ?OutputInterface $output = null): IngestReport
    {
        $this->logger->info(" Starting ingest process...");

        // Before init anything, lets check if the given file is OK
        $this->logFileService->performChecksOnFile($filePath);

        // The main 3 steps of the process
        $this->initializeIngestProcess($filePath, $output);
        $this->ingestRawLinesFromFile($filePath);
        $this->finalizeIngestProcess();

        return $this->report;
    }

    /**
     * @param string $filePath
     * @param OutputInterface|null $output
     */
    private
    function initializeIngestProcess(
        string $filePath,
        ?OutputInterface $output
    ): void {
        // Initialize ingest report
        $linesCount = $this->logFileService->checkAndReturnLinesCount($filePath);
        $this->report = new IngestReport($linesCount);

        // Initialize ProgressBar if output is available
        if (null !== $output) {
            $this->progressBar = new ProgressBar(
                $output,
                intval($this->report->getTotalLinesCount() / self::FLUSH_THRESHOLD)
            );

            $this->printDebug = $output->isDebug();
        }

        // Performance optimizations : disable SQL logger
        $this->registry->getManager()->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * @param string $filePath
     * @throws \Exception
     */
    private function ingestRawLinesFromFile(string $filePath)
    {
        $handle = fopen($filePath, "r");

        $rawLine = null;
        // For each line in the file, we try to save it into the database and if it fails, writes to an error file
        while (!feof($handle)) {
            try {
                $rawLine = fgets($handle);
                $rawLine ?: throw new \RuntimeException('Could not read line from file');
                $this->buildAndSaveLogLineFromRawLine($rawLine);
            } catch (\Exception $e) {
                $this->handleRawLineInError($filePath, $rawLine, (string)$e);
            }

            if (!$this->report->isFailureRateAcceptable()) {
                break;
            }
        }

        // Final flush to not forget last bunch of LogLines
        $this->registry->getManager()->flush();
        $this->progressBar?->finish();

        // Line return because progress bar does not do one at the end
        $this->logger->info('');

        fclose($handle);
    }

    /**
     * @throws \Exception
     */
    private
    function buildAndSaveLogLineFromRawLine(
        string $rawLine
    ): void {
        // Builds the line entity, validate it against annotations validators and save it if it is valid.
        $lineEntity = $this->logLineBuilder->buildFromRawLine($rawLine, $this->report->getUid());
        $this->validationService->validateEntity($lineEntity);
        $this->registry->getManager()->persist($lineEntity);
        $this->report->increaseSuccessLines();

        // Performance optimizations : flush and clear th UnitOfWork on a threshold to improve perfs
        if ($this->report->getTotalLinesTreated() % self::FLUSH_THRESHOLD === 0) {
            $this->registry->getManager()->flush();
            $this->registry->getManager()->clear();
            $this->progressBar?->advance();
        }
    }

    private
    function finalizeIngestProcess(): void
    {
        $this->report->setEndDate(new \DateTime());

        // Print report
        $this->logger->info('Finished ingest process ' . $this->report->getUid() . ' | Report :');
        $this->logger->info('Total lines         : ' . $this->report->getTotalLinesCount());
        $this->logger->info('Success lines       : ' . $this->report->getSuccessLinesCount());
        $this->logger->info('Failure lines       : ' . $this->report->getFailureLinesCount());
        $this->logger->info('Total time          : ' . $this->report->getTotalExecutionTimeToDisplay());
        $this->logger->info('Global Failure rate : ' . $this->report->computeFailureRate() . '%');

        // If status is failed then we have to delete those lines just added (maybe too rough)
        if ($this->report->getStatus() === IngestProcessStatusEnum::FAILED) {
            $this->logger->info(
                ' Failure reason: ' . $this->report->getFailedReason()
            );
        } else {
            $this->report->setStatus(IngestProcessStatusEnum::SUCCESS);
            $this->registry->getManager()->persist($this->report);
            $this->registry->getManager()->flush();
        }

        $this->logger->info('Status              : ' . $this->report->getStatus()->name);

        // Closes the error file resource and printing the path
        if ($this->errorsFile && $this->report->getFailureLinesCount() > 0) {
            $data = stream_get_meta_data($this->errorsFile);
            $closed = fclose($this->errorsFile);
            $closed && isset($data['uri']) ?
                $this->logger->info('Wrote file with lines in errors: ' . $data['uri']) :
                $this->logger->info('Error file writing failed.');
        }
    }

    /**
     * @param string $filePath
     * @param string|bool|null $rawLine
     * @param string|null $error
     */
    private
    function handleRawLineInError(
        string $filePath,
        string|bool|null $rawLine,
        ?string $error
    ): void {
        // Prepare errors file if not already there
        if (!$this->errorsFile) {
            $errorsFileName = $filePath . '.' . $this->report->getUid() . '.errors';
            $this->errorsFile = fopen($errorsFileName, "w");

            // This file should be written
            if (!$this->errorsFile) {
                if (null !== error_get_last() && isset(error_get_last()['message'])) {
                    $this->logger->error(
                        ' Error: during error file creation: ' . error_get_last()['message']
                    );
                } else {
                    $this->logger->error('Unknown error during error file creation');
                }
            }
        }

        // Writes the line to the file
        if ($this->errorsFile) {
            if ($this->printDebug) {
                $this->logger->error('!! Line : ' . $rawLine);
                $this->logger->error('!! Error : ' . $error);
            }
            $rawLine === null || $rawLine === false ?: fwrite($this->errorsFile, $rawLine);
        }

        // Add the failed line to report stats
        $this->report->increaseFailedLines();
    }
}