<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\LogLine;

class LogLineBuilder
{
    /**
     * @throws \Exception
     */
    public
    function buildFromRawLine(
        string $rawLine,
        string $reportUid
    ): LogLine {
        preg_match('/(.*)-SERVICE\ -\ -\ \[(.*)\ (.*)\]\ "(.*)"\ (\d+)/', $rawLine, $groups);

        if (count($groups) !== 6) {
            throw new \RuntimeException(
                'Line log did not pass regexp : needed 6 matching groups found ' . count($groups)
            );
        }

        return new LogLine($groups[1], $groups[2], $groups[3], $groups[4], $groups[5], $reportUid);
    }
}