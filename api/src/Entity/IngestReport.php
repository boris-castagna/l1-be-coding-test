<?php

declare(strict_types=1);

namespace App\Entity;

use App\Model\Enums\IngestProcessStatusEnum;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

#[ORM\Table]
#[ORM\Entity]
class IngestReport
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $uid;

    #[ORM\Column(type: 'datetime')]
    private DateTime $startDate;

    #[ORM\Column(type: 'datetime')]
    private DateTime $endDate;

    #[ORM\Column(type: 'integer')]
    private int $totalLinesCount = 0;

    #[ORM\Column(type: 'integer')]
    private int $successLinesCount = 0;

    #[ORM\Column(type: 'integer')]
    private int $failureLinesCount = 0;

    #[ORM\Column(type: 'string', enumType: IngestProcessStatusEnum::class)]
    private IngestProcessStatusEnum $status;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private string $failedReason;

    // The rate threshold at which an ingest process is considered unacceptable and must stop
    public const ACCEPTABLE_FAILURE_RATE = 20;

    public function __construct(int $totalLinesCount)
    {
        $this->startDate = new DateTime('now');
        $this->uid = Uuid::uuid4()->toString();
        $this->totalLinesCount = $totalLinesCount;
        $this->status = IngestProcessStatusEnum::IN_PROGRESS;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return IngestReport
     */
    public function setId(int $id): IngestReport
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $startDate
     * @return IngestReport
     */
    public function setStartDate(DateTime $startDate): IngestReport
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @param DateTime $endDate
     * @return IngestReport
     */
    public function setEndDate(DateTime $endDate): IngestReport
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalLinesCount(): int
    {
        return $this->totalLinesCount;
    }

    /**
     * @return int
     */
    public function getSuccessLinesCount(): int
    {
        return $this->successLinesCount;
    }

    /**
     * @return int
     */
    public function getFailureLinesCount(): int
    {
        return $this->failureLinesCount;
    }

    /**
     * @return IngestProcessStatusEnum
     */
    public function getStatus(): IngestProcessStatusEnum
    {
        return $this->status;
    }

    /**
     * @param IngestProcessStatusEnum $status
     * @return IngestReport
     */
    public function setStatus(IngestProcessStatusEnum $status): IngestReport
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFailedReason(): ?string
    {
        return $this->failedReason;
    }

    /**
     * @param string|null $failedReason
     * @return IngestReport
     */
    public function setFailedReason(?string $failedReason): IngestReport
    {
        $this->failedReason = $failedReason;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return IngestReport
     */
    public function setUid(string $uid): IngestReport
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return $this
     */
    public function increaseFailedLines(): IngestReport
    {
        $this->failureLinesCount++;
        return $this;
    }

    /**
     * @return $this
     */
    public function increaseSuccessLines(): IngestReport
    {
        $this->successLinesCount++;
        return $this;
    }

    /**
     * @return float
     */
    public function computeFailureRate(): float
    {
        return $this->getTotalLinesCount() === 0 ? 0 : round(
            $this->getFailureLinesCount() * 100 / $this->getTotalLinesCount(),
            2
        );
    }

    /**
     * @return bool
     */
    public function isFailureRateAcceptable(): bool
    {
        $failureRate = $this->computeFailureRate();
        if ($failureRate > self::ACCEPTABLE_FAILURE_RATE) {
            $this->setStatus(IngestProcessStatusEnum::FAILED);
            $this->setFailedReason(
                "Failure rate is too high ($failureRate%). Aborting ingestion of file..." .
                "WARNING! You should probably manually delete all files with this report's uid."
            );
            return false;
        }
        return true;
    }

    /**
     * @return string|null
     */
    public function getTotalExecutionTimeToDisplay(): ?string
    {
        $totalSeconds = $this->getEndDate()->getTimestamp() - $this->getStartDate()->getTimestamp();

        // Nice display, such as "60 seconds" or "15min 45secs" or "2h 45min 37secs"
        if ($totalSeconds === 0) {
            $diff = (int)$this->getEndDate()->diff($this->getStartDate())->format('%F');
            return $diff / 1000 . ' milliseconds';
        }

        if ($totalSeconds < 60) {
            return "$totalSeconds seconds";
        }

        if ($totalSeconds > 60 && $totalSeconds < 3600) {
            $minutes = floor($totalSeconds / 60);
            $seconds = floor(($totalSeconds - $minutes) / 60);
            return "$minutes min $seconds secs";
        }

        if ($totalSeconds >= 3600) {
            $hours = floor($totalSeconds / 3600);
            $minutes = floor(($totalSeconds - $hours * 3600) / 60);
            $seconds = floor($totalSeconds - $hours * 3600 - $minutes * 60);
            return "$hours h $minutes min $seconds secs";
        }

        return null;
    }


    /**
     * @return int
     */
    public function getTotalLinesTreated(): int
    {
        return $this->failureLinesCount + $this->successLinesCount;
    }
}
