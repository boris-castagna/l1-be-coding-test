<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table]
#[ORM\Entity]
class LogLine
{
    public const DATE_TIME_FORMAT = 'd/M/Y:H:i:s';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    private string $service;

    #[ORM\Column(type: 'datetime')]
    #[Assert\NotNull()]
    private \DateTime $date;

    #[ORM\Column(type: 'integer')]
    #[Assert\NotNull()]
    #[Assert\GreaterThanOrEqual(value: 100, message: 'HTTP Code must be greater than 100')]
    #[Assert\LessThanOrEqual(value: 599, message: 'HTTP Code must be lesser than 599')]
    private int $httpStatusCode;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $content;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $reportUid; // no foreign key here as it will impact INSERT performance

    /**
     * Implement Factory or builder pattern if this gets more complicated over time
     *
     * @param string $service
     * @param string $date
     * @param string $timeZone
     * @param string $content
     * @param string $httpStatusCode
     * @param string $reportUid
     * @throws \Exception
     */
    public function __construct(
        string $service,
        string $date,
        string $timeZone,
        string $content,
        string $httpStatusCode,
        string $reportUid
    ) {
        $this->setService($service);
        $this->setDateFromString($date, $timeZone);
        $this->httpStatusCode = (int)$httpStatusCode;
        $this->content = $content;
        $this->reportUid = $reportUid;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return LogLine
     */
    public function setId(int $id): LogLine
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @param string $service
     * @return LogLine
     */
    public function setService(string $service): LogLine
    {
        $this->service = mb_strtolower($service);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return LogLine
     */
    public function setDate(\DateTime $date): LogLine
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getHttpStatusCode(): int
    {
        return $this->httpStatusCode;
    }

    /**
     * @param int $httpStatusCode
     * @return LogLine
     */
    public function setHttpStatusCode(int $httpStatusCode): LogLine
    {
        $this->httpStatusCode = $httpStatusCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return LogLine
     */
    public function setContent(?string $content): LogLine
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReportUid(): ?string
    {
        return $this->reportUid;
    }

    /**
     * @param string|null $reportUid
     * @return LogLine
     */
    public function setReportUid(?string $reportUid): LogLine
    {
        $this->reportUid = $reportUid;
        return $this;
    }

    /**
     * @param string $date
     * @param string $timeZone
     * @param bool $throwOnError
     * @return LogLine
     */
    public function setDateFromString(string $date, string $timeZone, bool $throwOnError = true): LogLine
    {
        $date = \DateTime::createFromFormat(self::DATE_TIME_FORMAT, $date, new \DateTimeZone($timeZone));
        if (!$date && $throwOnError) {
            throw new \RuntimeException(
                'Error during date creation with format ' . self::DATE_TIME_FORMAT .
                'and value: ' . $date
            );
        }
        $this->date = $date;
        return $this;
    }
}
