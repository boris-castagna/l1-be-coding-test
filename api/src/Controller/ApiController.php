<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Dto\ApiCountDto;
use App\Model\Dto\ApiErrorDto;
use App\Model\Dto\ApiFiltersDto;
use App\Repository\LogLineRepository;
use App\Service\ValidationService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractFOSRestController
{
    /**
     * Searches logs and provides aggregated count of matches
     *
     * @OA\Tag(name="Analytics",
     *     description="Count all matching items in the logs")
     * @OA\Response(
     *     response=200,
     *     description="count of matching results",
     *     @Model(type=ApiCountDto::class)
     * )
     * @OA\Response(
     *    response=400,
     *     description="Bad input parameter"
     * )
     *
     * @Rest\Get("/api/count", name="search_logs")
     * @Rest\View()
     *
     * @Rest\QueryParam(name="serviceNames", map=true, nullable=true,
     *     description="serviceNames filtering, array. Use services names directly, example: 'user'")
     * @Rest\QueryParam(name="statusCode", requirements="\d+", nullable=true, description="statusCode filtering, between 100 & 599")
     * @Rest\QueryParam(name="startDate", nullable=true, description="startDate filtering, date as string (UTC), format d/M/Y:H:i:s")
     * @Rest\QueryParam(name="endDate", nullable=true, description="endDate filtering, date as string (UTC), format d/M/Y:H:i:s")
     *
     * @param ParamFetcher $fetcher
     * @param ValidationService $validationService
     * @param LogLineRepository $logLineRepository
     * @return View|ApiCountDto
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getLogsCount(
        ParamFetcher $fetcher,
        ValidationService $validationService,
        LogLineRepository $logLineRepository
    ): View|ApiCountDto {
        try {
            // builds and validates the filters Dto
            $filtersDto = new ApiFiltersDto(
                $fetcher->get('serviceNames'),
                $fetcher->get('statusCode'),
                $fetcher->get('startDate'),
                $fetcher->get('endDate')
            );
            $validationService->validateEntity($filtersDto);
        } catch (Exception $e) {
            // here we have to return fosrest view in order to control the http status code as we want
            return View::create(
                new ApiErrorDto('bad input parameter', $e->getMessage()),
                Response::HTTP_BAD_REQUEST
            );
        }

        // get the lines count
        $count = $logLineRepository->countLinesWithFilters($filtersDto);

        // return the result
        return new ApiCountDto($count);
    }
}