<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\LogLine;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GenerateLogsFileCommand extends Command
{
    protected static $defaultName = 'legalone:generate-logs-file';

    private const SERVICES_NAMES = [
        'USER',
        'INVOICE',
        'ACCOUNTING-AND-TAXES',
        'CUSTOMER',
        'CASE',
        'LAW-GERMANY',
        'FILES',
        'POLICE'
    ];
    private const HTTP_METHODS = ['GET', 'POST', 'PUT', 'PATCH'];
    private const HTTP_CODE = ['200', '204', '208', '404', '418', '403', '500', '0'];
    private const ENDPOINTS = ['/ressource', '/ressource/123/user', '/example/log', '/admin'];

    public function __construct(
        private ParameterBagInterface $params
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('lines', InputArgument::REQUIRED, 'The number of lines to generate in the file.');
    }

    /**
     * Generates a file with x number of lines in the form :
     * USER-SERVICE - - [18/Aug/2021:10:33:59 +0000] "POST /users HTTP/1.1" 201
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $lines = $input->getArgument('lines');

        if ($lines > PHP_INT_MAX) {
            throw new \RuntimeException('Well, this is a LOT of lines...');
        }

        $fileName = $this->params->get('kernel.project_dir') . '/var/test_logs_' . Uuid::uuid4()->toString() . '.txt';
        $testFile = fopen($fileName, "w");
        $maxTimestamp = (new \DateTime())->getTimestamp();
        $minTimestamp = $maxTimestamp - 400000;

        for ($i = 1; $i <= $lines; $i++) {
            $line = $this->getRandomValueInArray(self::SERVICES_NAMES) . '-SERVICE - - ' .
                '[' . $this->getRandomDateWithTimeZone($minTimestamp, $maxTimestamp) . '] ' .
                '"' . $this->getRandomValueInArray(self::HTTP_METHODS) . ' ' .
                $this->getRandomValueInArray(self::ENDPOINTS) . ' HTTP/1.1" ' .
                $this->getRandomValueInArray(self::HTTP_CODE);

            fwrite($testFile, $line . "\n");
        }

        fclose($testFile);

        $output->writeln('File generated : ' . $fileName);

        return Command::SUCCESS;
    }

    private function getRandomValueInArray(array $values): string
    {
        return $values[rand(0, count($values) - 1)];
    }

    private function getRandomDateWithTimeZone(int $minTimestamp, int $maxTimestamp)
    {
        return (new \DateTime())
                ->setTimestamp(rand($minTimestamp, $maxTimestamp))
                ->format(LogLine::DATE_TIME_FORMAT) . ' +0' . rand(0, 4) . '00';
    }
}