<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\IngestLogsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IngestLogsCommand extends Command
{
    protected static $defaultName = 'legalone:ingest-logs';

    public function __construct(
        private IngestLogsService $ingestLogsService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('file', InputArgument::REQUIRED, 'The Log file to be ingested.');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->ingestLogsService->ingestLogs($input->getArgument('file'), $output);
        return Command::SUCCESS;
    }
}