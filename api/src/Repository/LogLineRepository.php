<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\LogLine;
use App\Model\Dto\ApiFiltersDto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class LogLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogLine::class);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countLinesWithFilters(ApiFiltersDto $filtersDto): int
    {
        $qb = $this->createQueryBuilder('l');
        $qb->select('count(l.id)')
            ->where('1=1');

        if ($filtersDto->getServiceNames() && count($filtersDto->getServiceNames()) > 0) {
            $qb->andWhere(
                $qb->expr()->in(
                    'l.service',
                    $filtersDto->getServiceNames()
                )
            );
        }

        if ($filtersDto->getStatusCode()) {
            $qb->andWhere(
                $qb->expr()->eq(
                    'l.httpStatusCode',
                    $filtersDto->getStatusCode()
                )
            );
        }

        if ($filtersDto->getStartDate()) {
            $qb->andWhere(
                $qb->expr()->gte(
                    'l.date',
                    ':startDate'
                )
            )->setParameter('startDate', $filtersDto->getStartDate(), Types::DATETIME_MUTABLE);
        }

        if ($filtersDto->getEndDate()) {
            $qb->andWhere(
                $qb->expr()->lte(
                    'l.date',
                    ':endDate'
                )
            )->setParameter('endDate', $filtersDto->getEndDate(), Types::DATETIME_MUTABLE);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }
}